Para ejecutar la aplicación al completo:

docker-compose up -d --scale web=5

------------------------------------------------------------
Para terminar de trabajar con los contenedores:

docker-compose down

Para contruir la imagen:

docker build -t friendlyhello .

Para ejecutar la aplicación sin redis:

docker run -p 4000:80 friendlyhello
